module SubProcess

import Data.Maybe, Data.Func, Data.Error
import Gast, Gast.Gen, Gast.CommandLine
import StdEnv, StdOverloadedList
import System.SubProcess
import System.SysCall
import System._Posix, System._Unsafe
import Text.GenPrint

Start world = exposeProperties [OutputTestEvents] [Bent] properties world

properties :: [Property]
properties =:
    // `readFromCrashedSubProcTest` has to be done first, as it can pass if the state of an internal buffer is changed
    // already.
    [ readFromCrashedSubProcTest as "reading from a crashed subprocess yield the expected error"
    , echoTest as "subprocesses correctly echo messages back to parent"
    , waitTest as "waiting for a subprocess returns when it terminates"
    , terminationTest as "terminating a subprocess returns when it still running"
    , multiTerminationTest as "resources are freed correctly when forking and terminating a large number of subprocesses"
    , nProcsTest as "there is at least 1 processor available"
	, exitStatusTest as "test for C functions which use the `ExitStatus`"]

:: Message :== [!(String, ?Int)] // Some arbitrary (complex) type for test messages.

echoTest :: ![!Message] -> Property
echoTest msgs = accUnsafe echoTest`
where
    echoTest` :: !*World -> (!Property, !*World)
    echoTest` world
        # (Ok subProc, world) = forkSubProcess doSubProcess world
        # world = Foldl (\world msg -> snd $ send msg subProc.outChannel world) world msgs
        # (receivedMsgs, world) = receiveN subProc.inChannel (Length msgs) world
        # (Ok _, world) = terminate subProc world
        = (receivedMsgs =.= msgs, world)

    doSubProcess :: !(InChannel Message) !(OutChannel Message) !*World -> *World
    doSubProcess inChannel outChannel world
        # (msgs, world) = receiveN inChannel (Length msgs) world
        = Foldl (\world msg -> snd $ send msg outChannel world) world msgs

    receiveN :: !(InChannel Message) !Int !*World -> (![!Message], !*World)
    receiveN _ 0 world = ([!], world)
    receiveN pipe nrLeft world
        # (Ok msg, world) = receive pipe world
        # (rest, world) = receiveN pipe (dec nrLeft) world
        = ([!msg: rest], world)

waitTest :: Property
waitTest = accUnsafe waitTest`
where
    waitTest` :: !*World -> (!Property, !*World)
    waitTest` world
        # (Ok subProc, world) = forkSubProcess (\_ _ world -> world) world
        # (Ok _, world) = waitForSubProcessToTerminate subProc world
        = (prop True, world)

terminationTest :: !Bool -> Property
terminationTest blockReceivingMsg = accUnsafe waitTest`
where
    waitTest` :: !*World -> (!Property, !*World)
    waitTest` world
        # (Ok subProc, world) = forkSubProcess (if blockReceivingMsg block loop) world
        # (Ok _, world) = terminate subProc world
        = (prop True, world)

    // `receive` blocks and makes sure the subprocess is still running when terminated by the parent.
    block :: !(InChannel ()) !(OutChannel ()) !*World -> *World
    block ch _ world = snd $ receive ch world

    // loop forever
    loop :: x y !*World -> *World
    loop chIn chOut world = loop chIn chOut world

// Just check whether this works without crashing.
multiTerminationTest :: Property
multiTerminationTest = accUnsafe terminationTest`
where
    terminationTest` :: !*World -> (!Property, !*World)
    terminationTest` world = (prop True, Foldl forkAndTerminate world [1..nrsubprocesses])

    forkAndTerminate :: !*World x -> *World
    forkAndTerminate world _
        # (Ok subProc, world) = forkSubProcess (\_ _ world -> world) world
        # (Ok _, world) = terminate subProc world
        = world

    nrsubprocesses = 10000

//* The property may seem obvious, but still this checks whether the syscall succeeds.
nProcsTest :: Property
nProcsTest = accUnsafe nProcsTest`
where
    nProcsTest` :: !*World -> (!Property, !*World)
    nProcsTest` world
        # (nr, world) = getNumberOfAvailableProcessors world
        = (nr >. 0, world)

exitStatusTest :: Property
exitStatusTest = accUnsafe waitTest`
where
    waitTest` :: !*World -> (!Property, !*World)
    waitTest` world
        # (Ok subProc, world) = forkSubProcess (\_ _ world -> world) world
        # (Ok exitStatus, world) = waitForSubProcessToTerminate subProc world
		# exitCode = exitCodeIn exitStatus
		# signal = signalIn exitStatus
		# hadCoreDump = hadCoreDump exitStatus
        = (exitCode =.= ?Just 0 /\ signal =.= ?None /\ hadCoreDump =.= False, world)

readFromCrashedSubProcTest :: Property
readFromCrashedSubProcTest = accUnsafe readFromCrashedSubProcTest`
where
    readFromCrashedSubProcTest` :: !*World -> (!Property, !*World)
    readFromCrashedSubProcTest` world
        # (Ok subProc, world) = forkSubProcess doSubProcess world
        # (receivedMsgs, world) = receive subProc.inChannel world
        = (prop $ receivedMsgs =: (Error (OUTPUT_CHANNEL_CLOSED, _)), world)

    doSubProcess :: !(InChannel ()) !(OutChannel ()) !*World -> *World
    doSubProcess inChannel outChannel world =
        exit 1 world // Use `exit` to crash as `undef` writes to stdout which interferes with the test runner.
