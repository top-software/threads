# Concurrency

This is a library providing support for concurrently running subprocesses.
Subprocesses do not share memory with the parent process.
Communication is realised by means of message passing.
