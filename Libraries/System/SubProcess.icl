implementation module System.SubProcess

import Data.Error, Data.Func, Data.Functor, Data.Tuple, Data._Array
import graph_copy
import StdEnv
import System.OSError, System.Signal, System._Linux, System._Pointer, System._Posix, System._Signal
import qualified System._Process
import System._Unsafe

import code from "SubProcessInterface.o"

:: InChannel a =: InChannel Int // Contains the FD of a pipe.
:: OutChannel a =: OutChannel Int // Contains the FD of a pipe.
:: SubProcessId =: SubProcessId Int // Contains the PID of the process.
:: ExitStatus =: ExitStatus Int

instance == SubProcessId where
	(==) :: !SubProcessId !SubProcessId -> Bool
	(==) a b = code inline {eqI}

instance < SubProcessId where
	(<) :: !SubProcessId !SubProcessId -> Bool
	(<) a b = code inline {ltI}

forkSubProcess ::
	!.((InChannel .inp) -> .((OutChannel .out) -> .(*env -> *env))) !*env
	-> (!MaybeOSError (SubProcess .inp .out), !*env) | SysCallEnv env
forkSubProcess subprocessFunc env
	# (res, env) = createChannel env
	| isError res = (liftError res, env)
	# (parentIn, childOut) = fromOk res
	# (res, env) = createChannel env
	| isError res = (liftError res, env)
	# (childIn, parentOut) = fromOk res
	# (subprocessId, env) = fork env
	# subprocess = {subprocessId = SubProcessId subprocessId, inChannel = parentIn, outChannel = parentOut}
	| subprocessId < 0 = getLastOSError env
	| subprocessId == 0
		// this is the child process
		# (ret, env) = prctl1 PR_SET_PDEATHSIG SIGTERM env // terminate when the parent process dies
		| ret == -1 = getLastOSError env
		// close pipes to free resources (errors are ignored, as passing them back to the parent does not add too much)
		# (_, env) = closeInChannel parentIn env
		# (_, env) = closeOutChannel parentOut env
		# env = subprocessFunc childIn childOut env
		= (Ok subprocess, exit 0 env)
	// here the parent process continues (`subprocessId` is the forked child's ID)
	// close pipes to free resources
	# (err, env) = closeInChannel childIn env
	| isError err = (liftError err, env)
	# (err, env) = closeOutChannel childOut env
	| isError err = (liftError err, env)
	= (Ok subprocess, env)

forkSubProcessWithoutChannels :: !(*env -> *env) !*env -> (!MaybeOSError SubProcessId, !*env) | SysCallEnv env
forkSubProcessWithoutChannels subprocessFunc env
	# (subprocessId, env) = fork env
	| subprocessId < 0 = getLastOSError env
	| subprocessId == 0
		// this is the child process
		# (ret, env) = prctl1 PR_SET_PDEATHSIG SIGTERM env // terminate when the parent process dies
		| ret == -1 = getLastOSError env
		# env = subprocessFunc env
		= (Ok $ SubProcessId subprocessId, exit 0 env)
	// here the parent process continues (`subprocessId` is the forked child's ID)
	= (Ok $ SubProcessId subprocessId, env)

createChannel :: !*env -> (!MaybeOSError (InChannel .a, OutChannel .a), !*env) | SysCallEnv env
createChannel env
	# (res, env) = 'System._Process'._openPipePair False env
	| isError res = (liftError res, env)
	# (inChannel, outChannel) = fromOk res
	= (Ok (InChannel inChannel, OutChannel outChannel), env)

send :: !.a !(OutChannel .a) !*env -> (!MaybeOSError (), !*env) | SysCallEnv env
send msg (OutChannel pipe) env
	# encodedString = copy_to_string msg
	# encodedStringSize = size encodedString
	# (err, env) = writeIntPipe encodedStringSize pipe env
	| isError err = (liftError err, env)
	# (err, env) = writePipe encodedString pipe env
	| isError err = (liftError err, env)
	= (Ok (), env)
where
	writePipe :: !String !Int !*env -> (!MaybeOSError (), !*env) | SysCallEnv env
	writePipe data pipe env
		# (res, env) = write pipe data (size data) env
		| res == -1 = getLastOSError env
		= (Ok (), env)

	writeIntPipe :: !Int !Int !*env -> (!MaybeOSError (), !*env) | SysCallEnv env
	writeIntPipe data pipe env
		# (res, env) = writeInts pipe {#data} (IF_INT_64_OR_32 8 4) env
		| res == -1 = getLastOSError env
		= (Ok (), env)

receive :: !(InChannel .a) !*env -> (!MaybeOSError .a, !*env) | SysCallEnv env
receive  (InChannel pipe) env
	# intSize = IF_INT_64_OR_32 8 4
	# (count, env) = readInts pipe intBuffer intSize env
	| count == -1 = getLastOSError env
	| count <> intSize = (outputChannelClosedErr, env)
	# n = intBuffer.[0]
	# string = unsafeCreateArray n
	# (ret, env) = receiveNBytes pipe string env
	| ret < 0 = getLastOSError env
	| ret == 0 = (outputChannelClosedErr, env)
	= (Ok $ fst $ copy_from_string $ unsafeCoerce string, env)
where
	outputChannelClosedErr =
		Error (OUTPUT_CHANNEL_CLOSED, "Corresponding output channel closed while receiving the message")

	receiveNBytes :: !Int !String !*env -> (!Int, !*env)
	receiveNBytes int str env = code {
		ccall receiveNBytes "IS:I:A"
	}

peek :: !(InChannel .a) !*env -> (!MaybeOSError (? .a), !*env) | SysCallEnv env
peek ch=:(InChannel pipe) env
	# (res, env) = ioctl pipe FIONREAD buffer env
	| res == -1 = getLastOSError env
	# dataAvailable = readInt4Z buffer 0
	| dataAvailable == 0 = (Ok ?None, env)
	# (msg, env) = receive ch env
	= (mapMaybeError ?Just msg, env)

buffer :: Pointer
buffer =: accUnsafe $ malloc 4

setBufferSize :: !Int !(InChannel .a) !*env -> (!MaybeOSError Int, !*env) | SysCallEnv env
setBufferSize size (InChannel pipe) env
	# (ret, env) = fcntlArg pipe F_SETPIPE_SZ size env
	| ret == -1 = getLastOSError env
	= (Ok ret, env)

terminate :: !(SubProcess .inp .out) !*env -> (!MaybeOSError ExitStatus, !*env) | SysCallEnv env
terminate subprocess=:{subprocessId} env
	# (ret, env) = terminateBySubProcessId subprocessId env
	| isError ret = (ret, env)
	# (err, env) = closePipesOf subprocess env
	| isError err = (liftError err, env)
	= (ret, env)

waitForSubProcessToTerminate :: !(SubProcess .inp .out) !*env -> (!MaybeOSError ExitStatus, !*env) | SysCallEnv env
waitForSubProcessToTerminate subprocess=:{subprocessId} env
	# (ret, env) = waitForSubProcessToTerminateById subprocessId env
	| isError ret = (ret, env)
	# (err, env) = closePipesOf subprocess env
	| isError err = (liftError err, env)
	= (ret, env)

checkIfSubProcessTerminated :: !(SubProcess .inp .out) !*env -> (!MaybeOSError (?ExitStatus), !*env) | SysCallEnv env
checkIfSubProcessTerminated subprocess=:{subprocessId} env
	# (ret, env) = checkIfSubProcessTerminatedById subprocessId env
	| isError ret = (ret, env)
	| ret =: (Ok ?None) = (ret, env)
	# (err, env) = closePipesOf subprocess env
	| isError err = (liftError err, env)
	= (ret, env)

closePipesOf :: !(SubProcess .inp .out) !*env -> (!MaybeOSError (), !*env) | SysCallEnv env
closePipesOf {inChannel=(InChannel inChannel), outChannel=(OutChannel outChannel)} env
	# (err, env) = closePipe inChannel env
	| isError err = (liftError err, env)
	# (err, env) = closePipe outChannel env
	| isError err = (liftError err, env)
	= (Ok (), env)

closeInChannel :: !(InChannel .a) !*env -> (!MaybeOSError (), !*env) | SysCallEnv env
closeInChannel (InChannel pipe) env = closePipe pipe env

closeOutChannel :: !(OutChannel .a) !*env -> (!MaybeOSError (), !*env) | SysCallEnv env
closeOutChannel (OutChannel pipe) env = closePipe pipe env

closePipe :: !Int !*env -> (!MaybeOSError (), !*env) | SysCallEnv env
closePipe pipe env
	# (res, env) = close pipe env
	| res == -1 = getLastOSError env
	= (Ok (), env)

terminateBySubProcessId :: !SubProcessId !*env -> (!MaybeOSError ExitStatus, !*env) | SysCallEnv env
terminateBySubProcessId subprocessId=:(SubProcessId pid) env
	# (ret, env) = kill pid SIGTERM env
	| ret == -1 = getLastOSError env
	= waitForSubProcessToTerminateById subprocessId env

waitForSubProcessToTerminateById :: !SubProcessId !*env -> (!MaybeOSError ExitStatus, !*env) | SysCallEnv env
waitForSubProcessToTerminateById (SubProcessId subprocessId) env
	# (ret, env) = waitpid subprocessId intBuffer 0 env
	| ret == -1 = getLastOSError env
	= (Ok $ ExitStatus (intIn intBuffer), env)

checkIfSubProcessTerminatedById :: !SubProcessId !*env -> (!MaybeOSError (?ExitStatus), !*env) | SysCallEnv env
checkIfSubProcessTerminatedById (SubProcessId subprocessId) env
	# (ret, env) = waitpid subprocessId intBuffer WNOHANG env
	| ret == -1 = getLastOSError env
	= (Ok $ if (ret == 0) ?None (?Just $ ExitStatus (intIn intBuffer)), env)

waitForNextSubProcessToTerminate :: !*env -> (!MaybeOSError (SubProcessId, ExitStatus), !*env) | SysCallEnv env
waitForNextSubProcessToTerminate env
	# (pid, env) = wait intBuffer env
	| pid == -1 = getLastOSError env
	#! res = intIn intBuffer
	= (Ok (SubProcessId pid, ExitStatus res), env)

checkIfAnySubProcessTerminated :: !*env -> (!MaybeOSError (?(SubProcessId, ExitStatus)), !*env) | SysCallEnv env
checkIfAnySubProcessTerminated env
	# (pid, env) = waitpid -1 intBuffer WNOHANG env
	| pid == -1 = getLastOSError env
	| pid == 0 = (Ok ?None, env)
	#! res = intIn intBuffer
	= (Ok $ ?Just (SubProcessId pid, ExitStatus res), env)

intIn :: !{#Int} -> Int
// Exit code is 32-bit int, so ignore the first 32 bits on 64-bit machines.
intIn intBuffer = IF_INT_64_OR_32 (intBuffer.[0] bitand 0xffffffff) intBuffer.[0]

readInts :: !Int !{#Int} !Int !*env -> (!Int, !*env)
readInts fd buffer maxSize env = code {
	ccall read "IAI:I:A"
}

writeInts :: !Int !{#Int} !Int !*env -> (!Int, !*env)
writeInts fd buffer nBuffer world = code {
    ccall write "IAI:I:A"
}

intBuffer :: {#Int}
intBuffer =: unsafeCreateArray 1

getNumberOfAvailableProcessors :: !*env -> (!Int, !*env) | SysCallEnv env
getNumberOfAvailableProcessors env = get_nprocs env

exitCodeIn :: !ExitStatus -> ?Int
exitCodeIn status
	# exitCode = exitCodeIn_ status
	// Process exited abnormally.
	| exitCode == 256 = ?None
	| otherwise = ?Just exitCode

exitCodeIn_ :: !ExitStatus -> Int
exitCodeIn_ _ = code {
	ccall exitCodeIn "I:I"
}

signalIn :: !ExitStatus -> ?Int
signalIn exitStatus
	# signal = signalIn_ exitStatus
	| signal == 0 = ?None
	| otherwise = ?Just signal

signalIn_ :: !ExitStatus -> Int
signalIn_ _ = code {
	ccall signalIn "I:I"
}

hadCoreDump :: !ExitStatus -> Bool
hadCoreDump exitStatus = code {
	ccall hadCoreDump "I:I"
}

instance toString SubProcessId where
	toString (SubProcessId pid) = toString pid
