implementation module System.SubProcess.GenJSON

from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
import Text.GenJSON

from System.SubProcess import :: SubProcessId

derive JSONDecode SubProcessId
derive JSONEncode SubProcessId
