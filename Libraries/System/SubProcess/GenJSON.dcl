definition module System.SubProcess.GenJSON

from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode

from System.SubProcess import :: SubProcessId

derive JSONDecode SubProcessId
derive JSONEncode SubProcessId
