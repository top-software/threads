implementation module System.LockFile

import Data.Error
import Data.Func
import StdEnv
import System.FilePath, System.OSError
from System._Pointer import :: Pointer, packString
import System.SysCall
import System._Linux
import System._Posix

:: *LockFile = LockFile !Int // Contains the lockfile's file descriptor.

criticalSection :: !(*env -> (a, *env)) !*LockFile !*env -> (!MaybeOSError a, !*LockFile, !*env) | SysCallEnv env
criticalSection criticalFunc lfile=:(LockFile fd) env
    # (ret, env) = flock fd LOCK_EX env
    | ret <> 0
        # (err, env) = getLastOSError env
        = (err, lfile, env)
    # (res, env) = criticalFunc env
    # (ret, env) = flock fd LOCK_UN env
    | ret <> 0
        # (err, env) = getLastOSError env
        = (err, lfile, env)
    = (Ok res, lfile, env)

withLockFile ::
	!FilePath !(*LockFile -> *(*env -> (a, *LockFile, *env))) !*env -> (!MaybeOSError a, !*env) | SysCallEnv env
withLockFile path func env
    # (fd, env) = opens (packString path) O_CREAT MODE env
    | fd == -1 = getLastOSError env
    # (res, LockFile fd, env) = func (LockFile fd) env
    # (ret, env) = close fd env
    | ret <> 0 = getLastOSError env
    = (Ok res, env)
where
    MODE = 436 // Decimal representation of default mode (`664` in octal).
