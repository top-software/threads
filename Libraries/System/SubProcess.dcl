definition module System.SubProcess

from Data.Error import :: MaybeError
from StdEnv import class <
from System.OSError import :: MaybeOSError, :: OSError, :: OSErrorMessage, :: OSErrorCode
from System.SysCall import class SysCallEnv

//* A subprocess with corresponding communication channels.
:: SubProcess inp out = {subprocessId :: !SubProcessId, inChannel :: !InChannel out, outChannel :: !OutChannel inp}

//* Uniquely identifies a subprocess.
:: SubProcessId (=: SubProcessId Int)

//* Exit status of the process (not to be confused with exit code).
:: ExitStatus (=: ExitStatus Int)

instance == SubProcessId :: !SubProcessId !SubProcessId -> Bool :== code {eqI}

instance < SubProcessId :: !SubProcessId !SubProcessId -> Bool :== code {ltI}

/**
 * A typed channel for receiving messages of type `a`.
 * (The type should be abstract, but has to be included to support unique types, due to compiler limitations.)
 */
:: InChannel a =: InChannel Int

/**
 * A typed channel for sending messages of type `a`.
 * (The type should be abstract, but has to be included to support unique types, due to compiler limitations.)
 */
:: OutChannel a =: OutChannel Int

/**
 * Forks a subprocess, providing communication channels.
 * @param The forked subprocess function provided with channels to communicate with the parent process.
 * @param An environment.
 * @result The subprocess (including communication channels).
 */
forkSubProcess ::
	!.((InChannel .inp) -> .((OutChannel .out) -> .(*env -> *env))) !*env
	-> (!MaybeOSError (SubProcess .inp .out), !*env) | SysCallEnv env

/**
 * Forks a subprocess without communication channels.
 * The subprocess can still communicate with other subprocesss using their communication channels
 * or via another subprocess-safe mechanism provided by the environment (e.g. network connections, databases, ...).
 * @param The forked subprocess function.
 * @param An environment.
 * @result The subprocess ID.
 */
forkSubProcessWithoutChannels :: !(*env -> *env) !*env -> (!MaybeOSError SubProcessId, !*env) | SysCallEnv env

/**
 * Create a channel to communicate between subprocesses. This must be done before forking off a subprocess. Usually,
 * either the `InChannel` or the `OutChannel` is used in a subprocess. The other channel should be closed using
 * {{`closeInChannel`}}/{{`closeOutChannel`}} to free resources.
 */
createChannel :: !*env -> (!MaybeOSError (InChannel .a, OutChannel .a), !*env) | SysCallEnv env

/**
 * Sends a message to a communication channel.
 * This may block until the message is received.
 * Circles of subprocesss waiting on each other to receive messages have to be avoided to prevent deadlocks!
 * @param The message.
 * @param The channel.
 * @param An environment.
 */
send :: !.a !(OutChannel .a) !*env -> (!MaybeOSError (), !*env) | SysCallEnv env

/**
 * Receives a message from a communication channel.
 * This blocks if no message in sent to the channel until a message becomes available.
 * @param The channel.
 * @param An environment.
 * @result The oldest message sent to the channel. The custom error code `OUTPUT_CHANNEL_CLOSED` indicates that the
 *         corresponding output channel closed, which usually indicates that the process which is communicated with
 *         has terminated.
 */
receive :: !(InChannel .a) !*env -> (!MaybeOSError .a, !*env) | SysCallEnv env

//* Indicates that the output channel was closed.
OUTPUT_CHANNEL_CLOSED :== -1

/**
 * Peeks for a message on a communuication channel and receives it if it is present.
 * This only blocks in case of and as long as a message has been sent partially.
 * @param The channel.
 * @param An environment.
 * @result The oldest message send to the channel if present.
 */
peek :: !(InChannel .a) !*env -> (!MaybeOSError (? .a), !*env) | SysCallEnv env

/**
 * Set a channel's buffer size in bytes. As sending messages can block in case the buffer is full, increasing the size
 * may improve performance if a large amount of data is sent over the buffer. On a recent Linux system the default size
 * is 16*4KB. The size may be rounded up; the actual size is provided as a result. The maximal usable size is
 * determined by OS limits.
 * @param The buffer size in bytes.
 * @param The input channel to change the buffer size for.
 * @param An environment.
 * @result The actual buffer size; error in case the size cannot be set (due to limits, ...).
 */
setBufferSize :: !Int !(InChannel .a) !*env -> (!MaybeOSError Int, !*env) | SysCallEnv env

/**
 * Terminates a subprocess. This is required to free all resources used.
 * @param The subprocess to terminate.
 * @param An environment.
 * @result The process's exit status.
 */
terminate :: !(SubProcess .inp .out) !*env -> (!MaybeOSError ExitStatus, !*env) | SysCallEnv env

/**
 * Waits for a subprocess to terminate. This blocks until the subprocess has terminated.
 * All resources are freed when this function returns.
 * @param The subprocess to wait for.
 * @param An environment.
 * @result The process's exit status.
 */
waitForSubProcessToTerminate :: !(SubProcess .inp .out) !*env -> (!MaybeOSError ExitStatus, !*env) | SysCallEnv env

/**
 * Checks if a subprocess has terminated. This does not block. All resources are freed when this function returns,
 * if the process has terminated.
 * @param The subprocess to wait for.
 * @param An environment.
 * @result `Error` if the underlying system call failed, `Ok (?Just exitStatus)` if the subprocess terminated.
 */
checkIfSubProcessTerminated :: !(SubProcess .inp .out) !*env -> (!MaybeOSError (?ExitStatus), !*env) | SysCallEnv env

/**
 * Terminates a subprocess without communication channels. This is required to free all resources used.
 * Do not use this on the ID of a subprocess with communication channels, as not all resources are freed in this case.
 * Use {{`terminate`}} instead.
 * @param The ID of the subprocess to terminate.
 * @param An environment.
 * @result The process's exit status.
 */
terminateBySubProcessId :: !SubProcessId !*env -> (!MaybeOSError ExitStatus, !*env) | SysCallEnv env

/**
 * Waits for a subprocess without communication channels to terminate. This blocks until the subprocess has terminated.
 * Do not use this on the ID of a subprocess with communication channels, as not all resources are freed in this case.
 * Use {{`waitForSubProcessToTerminate`}} instead.
 * @param The ID of the subprocess to wait for.
 * @param An environment.
 * @param The process's exit status.
 */
waitForSubProcessToTerminateById :: !SubProcessId !*env -> (!MaybeOSError ExitStatus, !*env) | SysCallEnv env

/**
 * Checks if a subprocess without communication channels has terminated. This does not block.
 * Do not use this on the ID of a subprocess with communication channels, as not all resources are freed in this case.
 * Use {{`checkIfSubProcessTerminated`}} instead.
 * @param The ID of the subprocess to wait for.
 * @param An environment.
 * @result `Error` if the underlying system call failed, `Ok (?Just exitStatus)` if the subprocess terminated.
 */
checkIfSubProcessTerminatedById :: !SubProcessId !*env -> (!MaybeOSError (?ExitStatus), !*env) | SysCallEnv env

/**
 * Waits for the next running subprocess, which was started by the program, to terminate. If having subprocesses with
 * communication channels, not all resources are freed. Use {{`closeInChannel`}} and {{`closeOutChannel`}} on the
 * channels in this case.
 * @param An environment.
 * @param The process's ID and exit status.
 */
waitForNextSubProcessToTerminate :: !*env -> (!MaybeOSError (SubProcessId, ExitStatus), !*env) | SysCallEnv env

/**
 * Checks if any subprocess, which was started by the program, has terminated. If having subprocesses with
 * communication channels, not all resources are freed. Use {{`closeInChannel`}} and {{`closeOutChannel`}} on the
 * channels in this case.
 * @param An environment.
 * @result If any subprocess terminated, the process's ID and exit status of any subprocess that has terminated.
 */
checkIfAnySubProcessTerminated :: !*env -> (!MaybeOSError (?(SubProcessId, ExitStatus)), !*env) | SysCallEnv env

/**
 * Closes an {{`InChannel`}}. This should only be used on channels which are not used anymore and not on channels
 * included in {{`SubProcess`}} values.
 */
closeInChannel :: !(InChannel .a) !*env -> (!MaybeOSError (), !*env) | SysCallEnv env

/**
 * Closes an {{`OutChannel`}}. This should only be used on channels which are not used anymore and not on channels
 * included in {{`SubProcess`}} values.
 */
closeOutChannel :: !(OutChannel .a) !*env -> (!MaybeOSError (), !*env) | SysCallEnv env

/**
 * Gets the number of processors available.
 * This is useful for determining how many subprocesss to use for efficiently parallelising an operation.
 * The value may change, e.g. on hotpluggable systems.
 * @param An environment.
 * @result The number of processors.
 */
getNumberOfAvailableProcessors :: !*env -> (!Int, !*env) | SysCallEnv env

/**
 * Returns the exit code of a terminated subprocess if it terminated normally.
 *
 * @param The `ExitStatus` of the subprocess.
 * @result The exit code of the subprocess, if it terminated normally.
 */
exitCodeIn :: !ExitStatus -> ?Int

/**
 * Returns the signal number which caused a subprocess to terminate, if applicable.
 *
 * @param The `ExitStatus` of the subprocess.
 * @result `?Just` the signal number of the signal which caused to subprocess to terminate,
 *          or `?None` if the subprocess was not terminated by a signal.
 */
signalIn :: !ExitStatus -> ?Int

/**
 * Returns whether the subprocess produced a coredump.
 *
 * @param The `ExitStatus` of the subprocess.
 * @result `True` if the subprocess produced a coredump, `False` otherwise.
 */
hadCoreDump :: !ExitStatus -> Bool

instance toString SubProcessId
