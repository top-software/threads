definition module System.LockFile

/**
 * This provides lockfiles for synchronising the work of concurrent processes. Lockfiles have the following advantages:
 *     - They remains persistent at the system level, so they are not only applicable to synchronise subprocesses of a
 *       single process.
 *     - Locks are removed when processes terminate. So processes terminating within critical sections do not cause
 *       deadlocks.
 *     - Lockfiles can be shared between containers when created on a shared mount.
 */

from System.FilePath import :: FilePath
from System.OSError import :: MaybeOSError, :: MaybeError, :: OSError, :: OSErrorMessage, :: OSErrorCode
from System.SysCall import class SysCallEnv

//* A lockfile providing exclusive access to critical sections.
:: *LockFile

/**
 * Evalutes a function as critical section, such that only a single process is guaranteed to be active for the same
 * lockfile.
 * @param A function on an arbitrary environment to evaluate.
 * @param The lockfile.
 * @param An arbitrary environment.
 * @result The critical section's result.
 */
criticalSection :: !(*env -> (a, *env)) !*LockFile !*env -> (!MaybeOSError a, !*LockFile, !*env) | SysCallEnv env

/**
 * Performs a function with a lockfile. After completion resources are freed for the current process, but the lockfile
 * remains intact to be used by other processes.
 * @param The path to the lockfile.
 * @param The function to perform with the lockfile on an arbitrary environment.
 * @param An arbitrary environment.
 * @result The result obtained by the function using the lockfile.
 */
withLockFile ::
	!FilePath !(*LockFile -> *(*env -> (a, *LockFile, *env))) !*env -> (!MaybeOSError a, !*env) | SysCallEnv env
