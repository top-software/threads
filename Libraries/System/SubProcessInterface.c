#include <unistd.h>
#include <sys/wait.h>

#if defined(__x86_64__)
    #include "../../nitrile-packages/linux-x64/base-rts/misc/src/Clean.h"
#else
    #include "../../nitrile-packages/linux-x86/base-rts/misc/src/Clean.h"
#endif

int receiveNBytes(int pipe, CleanString cleanString) {
    size_t bytesLeft = CleanStringLength(cleanString);
    char* string = CleanStringCharacters(cleanString);
    while(bytesLeft) {
        const ssize_t r = read(pipe, string, bytesLeft);
        if (r <= 0) return r;
        bytesLeft -= r;
        string += r;
    }
}

int exitCodeIn(int status) {
	// WEXITSTATUS is max 8 bits, so 256 is used to indicate the process not having exited normally.
	return WIFEXITED(status) ? WEXITSTATUS(status) : 256;
}

int signalIn(int status) {
	return WIFSIGNALED(status) ? WTERMSIG(status) : 0;
}

int hadCoreDump(int status) {
#ifdef WCOREDUMP
	return WIFSIGNALED(status) ? WCOREDUMP(status) : 0;
# else
	return 0;
#endif
}
