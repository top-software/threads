# Changelog

## 4.0.0

- Feature: add `exitCodeIn` returning the exit code of a terminated process given an `ExitStatus`.
- Feature: add `signalIn` returning the number of the signal that caused the process to terminate, if applicable.
- Feature: add `hadCoreDump` returning if the terminated process produced a coredump.
- Change: `terminate` now returns an `ExitStatus` instead of an `Int`.
- Change: `terminateBySubProcessId` now returns an `ExitStatus` instead of an `Int`.
- Change: `waitForSubProcessToTerminate` now returns an `ExitStatus` instead of an `Int`.
- Change: `waitForNextSubProcessToTerminate` now returns an `ExitStatus` instead of an `Int`.
- Change: `waitForSubProcessToTerminateById` now returns an `ExitStatus` instead of an `Int`.
- Change: `checkIfSubProcessTerminated` now returns an `ExitStatus` instead of an `Int`.
- Change: `checkIfAnySubProcessTerminated` now returns an `ExitStatus` instead of an `Int`.
- Change: `checkIfSubProcessTerminatedById` now returns an `ExitStatus` instead of an `Int`.

#### 3.3.2

- Fix: include `SubProcessInterface.o` in package.

#### 3.3.1

- Enhancement: optimise `receive` for large messages.

### 3.3.0

- Feature: add `checkIfSubProcessTerminated`, `checkIfSubProcessTerminatedById` and `checkIfAnySubProcessTerminated`.
- Feature: add `==` and `<` instance for `SubProcessId`.

### 3.2.0

- Feature: add `waitForNextSubProcessToTerminate`.

#### 3.1.1

- Chore: support base `3.0`.

### 3.1.0

- Feature: Add json derivations for `:: SubProcessId`.

#### 3.0.2

- Fix: occasional garbage messages provided by `receive` in case sub-processes terminate.

#### 3.0.1

- Chore: support containers ^2.

## 3.0.0

- Change: support base 2.0 and require an instance for the `SysCallEnv` class for
          environments involved in functions which perform syscalls.

#### 2.2.1

- Chore: update graph-copy dependency.

### 2.2.0

- Feature: Add `createChannel`, `closeInChannel` and `closeOutChannel`, enabling more customised usage of channels for
           communication between subprocesses.
- Feature: Add `setBufferSize` and leave buffer size on default when creating channels, enabling more control of buffer
           sizes to tune performance.

### 2.1.0

- Chore: Add `toString` instance for `:: SubProcessId`.

## 2.0.0

- Feature: provide sub-processes' exit code when terminating/waiting for termination.

#### 1.3.1

- Fix: fix mode of lockfiles created by `LockFile.withLockFile`.

### 1.3.0

- Enhancement: document `receive` output channel closed error and add `OUTPUT_CHANNEL_CLOSED` constant.

### 1.2.0

- Feature: add a `LockFile` module for synchronising the work of concurrent processes.

### 1.1.0

- Feature: add a `peek` function for messages.
- Enhancement: allow to use messages of unique types.
- Fix: do not close pipes before waiting for a process to terminate.
- Fix: terminate process before closing pipes, to allow processes to gracefully handle term signals.
- Fix: `receive` yields an error when there is no data on the pipe (e.g. when it is closed); prevents a busy infinite loop.

## 1.0.0

- Initial version
